package com.unicamp.mc322.lab07;

import java.util.ArrayList;

class Board {

    // Static Properties
    private static String emptyRepresentation = "--";

    // Properties
    private Matrix<BoardItem> matrix;
    private Frog frog;
    private ArrayList<Obstacle> obstacles;
    private ArrayList<Food> foods;

    // Constructor
    Board(int rows, int cols, Frog frog) {
        this.matrix = new Matrix<BoardItem>(rows, cols, Board.emptyRepresentation);
        this.frog = frog;
        this.obstacles = new ArrayList<Obstacle>();
        this.foods = new ArrayList<Food>();
        this.setStartValueInMatrix(frog.currentPosition, frog);
    }

    // Methods
    public void print() {
        this.matrix.print();
    }

    public String getFrogName() {
        return this.frog.getName();
    }

    public double getScore() {
        return this.frog.getScore();
    }

    public boolean makeAMove(Matrix.Direction direction) {
        Position destination = this.frog.whereToMove(direction);
        boolean didGameEnd = false;

        if (this.matrix.isInBounds(destination)) {
            InteractiveItem item = this.getInteractiveItem(destination);

            if (item != null) {
                didGameEnd = item.doesGameEnd();
                item.interactWith(this.frog);
            }

            if (!didGameEnd) {
                this.moveFrog(destination);
            }

        } else {
            System.out.println("Ooh No! Out of bounds!");
            didGameEnd = true;
        }

        if (didGameEnd) {
            System.out.println("Game Over!");
        }

        return didGameEnd;
    }

    private void moveFrog(Position destination) {
        Position from = this.frog.getCurrentPosition();
        this.matrix.setValue(destination.row, destination.col, this.frog);
        this.matrix.setValue(from.row, from.col, null);
        frog.move(destination);
    }

    private InteractiveItem getInteractiveItem(Position position) {
        BoardItem item = this.matrix.getValue(position.row, position.col);
        if (item instanceof InteractiveItem) {
            InteractiveItem intItem = (InteractiveItem) item;
            return intItem;
        } else {
            return null;
        }
    }

    public void addObstacle(Obstacle obstacle) {
        Position[] positions = obstacle.getPositions();

        for (Position position: positions) {
            this.setStartValueInMatrix(position, obstacle);
        }

        this.obstacles.add(obstacle);
    }

    public void addFood(Food food) {
        Position position = food.getPosition();
        this.setStartValueInMatrix(position, food);
        this.foods.add(food);
    }

    private void setStartValueInMatrix(Position position, BoardItem item) {
        BoardItem oldItem = this.matrix.getValue(position.row, position.col);
        this.checkForItemSubstitution(position, oldItem);
        this.matrix.setValue(position.row, position.col, item);
    }

    private void checkForItemSubstitution(Position position, BoardItem item) {
        if (item != null) {
            throw new IllegalArgumentException("Trying to add BoardItem to occupied position: " + position);
        }
    }
}
