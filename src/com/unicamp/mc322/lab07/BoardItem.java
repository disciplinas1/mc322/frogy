package com.unicamp.mc322.lab07;

abstract class BoardItem {

    // Properties
    protected String symbol;

    // Constructor
    BoardItem(String symbol) {
        this.symbol = symbol;
    }

    // Methods
    @Override
    public String toString() {
        return this.symbol;
    }
}
