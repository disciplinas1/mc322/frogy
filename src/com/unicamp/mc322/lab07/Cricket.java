package com.unicamp.mc322.lab07;

class Cricket extends Food {

    // Static Properties
    private static String symbol = "gr";
    private static double points = 20;

    // Constructor
    Cricket(int row, int col) {
        super(Cricket.symbol, row, col, Cricket.points);
    }
}
