package com.unicamp.mc322.lab07;

class Firefly extends Food {

    // Static Properties
    private static String symbol = "va";
    private static double points = 15;

    // Constructor
    Firefly(int row, int col) {
        super(Firefly.symbol, row, col, Firefly.points);
    }
}
