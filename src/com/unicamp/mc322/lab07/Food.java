package com.unicamp.mc322.lab07;

abstract class Food extends InteractiveItem {

    // Properties
    protected Position position;
    protected double points;

    // Constructor
    Food(String symbol, int row, int col, double points) {
        super(symbol);
        this.position = new Position(row, col);
        this.points = points;

        if (this.position.hasNegativeCoordinates()) {
            throw new IllegalArgumentException("Food with negative coordinate: " + this.position);
        }
    }

    // Methods
    public Position getPosition() {
        return this.position;
    }

    public double getPoints() {
        return this.points;
    }

    @Override
    public void interactWith(Frog frog) {
        frog.eatFood(this);
    }

    @Override
    public boolean doesGameEnd() {
        return false;
    }
}
