package com.unicamp.mc322.lab07;

import java.util.Random;

abstract class Frog extends BoardItem {

    // Properties
    protected Position currentPosition;
    protected Position previousPosition;
    protected String name;
    protected double points;

    // Constructor
    Frog(String symbol, String name, int row, int col) {
        super(symbol);
        this.name = name;
        this.currentPosition = new Position(row, col);
        this.previousPosition = null;
        this.points = 0.0;
    }

    // Methods
    public void move(Position destination) {
        this.previousPosition = this.currentPosition;
        this.currentPosition = destination;
    }

    public void eatFood(Food food) {
        double rawPoints = food.getPoints();
        this.points += this.calculatePoints(rawPoints);
    }

    public String getName() {
        return this.name;
    }

    public double getScore() {
        return this.points;
    }

    public Position getCurrentPosition() {
        return this.currentPosition;
    }

    public Position getPreviousPosition() {
        return this.previousPosition;
    }

    // Abstract Methods
    public abstract Position whereToMove(Matrix.Direction direction);

    protected abstract double calculatePoints(double rawPoints);

    // Static Methods
    public static int pickRandomRange(int[] possibleRanges) {
        Random picker = new Random();
        return possibleRanges[picker.nextInt(possibleRanges.length)];
    }
}
