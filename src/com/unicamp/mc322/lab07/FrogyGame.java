package com.unicamp.mc322.lab07;

class FrogyGame {

    // Properties
    private InputReader input;

    // Constructor
    FrogyGame() {
        this.input = new InputReader();
    }

    // Enumerators
    enum FrogType {
        GREEN("Green"),
        TOMATO("Tomato"),
        POISONOUS("Poisonous");

        String label;

        FrogType(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return this.label;
        }
    }

    public void play() {
        Board board = this.initGame();
        boolean didGameEnd = false;
        System.out.println("Let the game begin!");

        while(!didGameEnd) {
            board.print();
            Matrix.Direction move = this.chooseMoveDirection();
            didGameEnd = board.makeAMove(move);
        }

        System.out.println("Name: " + board.getFrogName());
        System.out.println("Final Score: " + board.getScore());
    }

    private Board initGame() {
        String symbol = this.chooseSymbol();
        String name = this.chooseName();
        FrogType frogType = this.chooseFrogType();

        String[] boardOptions = new String[]{"Small" , "Large"};
        int boardOption = this.input.getOption("board size", boardOptions);

        Board board;
        switch (boardOption) {
            case 0:
                board = this.createSmallBoard(symbol, name, frogType);
                break;

            case 1:
            board = this.createLargeBoard(symbol, name, frogType);
                break;

            default:
            board = this.createSmallBoard(symbol, name, frogType);
                break;
        }

        return board;
    }

    private Board createSmallBoard(String frogSymbol, String frogName, FrogType frogType) {
        int rows = 6;
        int cols = 6;
        Position frogPosition = new Position(4, 2);
        Food cricket = new Cricket(1, 0);
        Food firefly = new Firefly(3, 5);
        Obstacle trap = new Trap(2, 3, 2, 5, 1, 4);

        Frog frog = this.createFrog(frogSymbol, frogName, frogType, frogPosition);
        Board board = new Board(rows, cols, frog);
        board.addFood(cricket);
        board.addFood(firefly);
        board.addObstacle(trap);

        return board;
    }

    private Board createLargeBoard(String frogSymbol, String frogName, FrogType frogType) {
        int rows = 10;
        int cols = 10;
        Position frogPosition = new Position(8, 7);
        Obstacle rock1 = new Rock(2, 7);
        Obstacle rock2 = new Rock(3, 2);
        Obstacle rock3 = new Rock(7, 1);
        Obstacle rock4 = new Rock(8, 4);
        Obstacle rock5 = new Rock(8, 8);
        Obstacle predator = new Predator(4, 4, 5, 5);
        Obstacle trap = new Trap(0, 0, 0, 1);
        Food firefly = new Firefly(1, 3);
        Food cricket = new Cricket(4, 7);

        Frog frog = this.createFrog(frogSymbol, frogName, frogType, frogPosition);
        Board board = new Board(rows, cols, frog);
        board.addObstacle(rock1);
        board.addObstacle(rock2);
        board.addObstacle(rock3);
        board.addObstacle(rock4);
        board.addObstacle(rock5);
        board.addObstacle(predator);
        board.addObstacle(trap);
        board.addFood(firefly);
        board.addFood(cricket);

        return board;
    }

    private Frog createFrog(String symbol, String name, FrogType type, Position position) {
        Frog frog;

        switch (type) {
            case GREEN:
                frog = new GreenFrog(symbol, name, position.row, position.col);
                break;

            case TOMATO:
                frog = new TomatoFrog(symbol, name, position.row, position.col);
                break;

            case POISONOUS:
                frog = new PoisonousFrog(symbol, name, position.row, position.col);
                break;

            default:
                frog = new GreenFrog(symbol, name, position.row, position.col);
                break;
        }

        return frog;
    }

    private FrogType chooseFrogType() {
        int choice = this.input.getOption("frog type", FrogType.values());
        return FrogType.values()[choice];
    }

    private String chooseSymbol() {
        System.out.println("Write your symbol in game");
        System.out.println("It's recommended a 2-char symbol");
        return this.input.getString();
    }

    private String chooseName() {
        System.out.println("Write your username");
        return this.input.getString();
    }

    private Matrix.Direction chooseMoveDirection() {
        boolean didMove = false;
        Matrix.Direction direction = null;

        while(!didMove) {
            String move = this.input.getString();

            switch (move.toLowerCase()) {
                case "w":
                    direction = Matrix.Direction.UP;
                    didMove = true;
                    break;

                case "a":
                    direction = Matrix.Direction.LEFT;
                    didMove = true;
                    break;

                case "s":
                    direction = Matrix.Direction.DOWN;
                    didMove = true;
                    break;

                case "d":
                    direction = Matrix.Direction.RIGHT;
                    didMove = true;
                    break;

                default:
                    didMove = false;
                    break;
            }
        }

        return direction;
    }
}
