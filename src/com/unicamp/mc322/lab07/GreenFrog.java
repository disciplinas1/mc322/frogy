package com.unicamp.mc322.lab07;

class GreenFrog extends Frog {

    // Constructor
    GreenFrog(String symbol, String name, int row, int col) {
        super(symbol, name, row, col);
    }

    // Methods
    @Override
    public Position whereToMove(Matrix.Direction direction) {
        return this.currentPosition.add(direction.variation);
    }

    @Override
    protected double calculatePoints(double rawPoints) {
        return 1.00 * rawPoints;
    }
}
