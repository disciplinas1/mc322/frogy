package com.unicamp.mc322.lab07;

import java.util.Scanner;

class InputReader {

    // Properties
    private Scanner keyboard;

    // Constructor
    InputReader() {
        this.keyboard = new Scanner(System.in);
    }

    // Methods
    public <T> int getOption(String label, T[] options) {
        boolean didChoose = false;
        int choice = -1;

        while (!didChoose) {
            System.out.println("Choose a " + label + ":");
            for (int index = 0; index < options.length; index++) {
                System.out.println(index + " - " + options[index]);
            }

            choice = this.keyboard.nextInt();

            if (choice < 0 || choice >= options.length) {
                System.out.println("Wrong option, try again");
            } else {
                didChoose = true;
            }
        }

        return choice;
    }

    public String getString() {
        String string = "";
        while (string.isEmpty()) {
            string = this.keyboard.nextLine();
        }

        return string;
    }

    public int getInt() {
        return this.keyboard.nextInt();
    }

}
