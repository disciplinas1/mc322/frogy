package com.unicamp.mc322.lab07;

abstract class InteractiveItem extends BoardItem {

    // Constructor
    InteractiveItem(String symbol) {
        super(symbol);
    }

    // Abstract Methods
    public abstract void interactWith(Frog frog);

    public abstract boolean doesGameEnd();
}
