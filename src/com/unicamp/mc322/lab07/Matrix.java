package com.unicamp.mc322.lab07;

import java.util.ArrayList;
import java.util.Random;

class Matrix<T> {

    // Properties
    private int rows;
    private int cols;
    private ArrayList<ArrayList<T>> matrix;
    private String nullRepresentation;

    // Constructors
    Matrix(int rows, int cols) {
        this(rows, cols, "null");
    }

    Matrix(int rows, int cols, String nullRepresentation) {
        if (rows < 1 || cols < 1) {
            throw new IllegalArgumentException("Invalid matrix size: " + new Position(rows, cols));
        }

        this.rows = rows;
        this.cols = cols;
        this.nullRepresentation = nullRepresentation;
        this.matrix = new ArrayList<ArrayList<T>>(rows);

        for (int i = 0; i < rows; i++) {
            ArrayList<T> row = new ArrayList<T>();

            for (int j = 0; j < cols; j++) {
                row.add(null);
            }

            this.matrix.add(row);
        }
    }

    // Enumerators
    enum Direction {
        UP(-1, 0),
        DOWN(1, 0),
        LEFT(0, -1),
        RIGHT(0, 1);

        // Properties
        Position variation;

        // Constructor
        Direction(int row, int col) {
            this.variation = new Position(row, col);
        }
    }

    // Methods
    public boolean isInBounds(Position position) {
        boolean inRowBounds = 0 <= position.row && position.row < this.rows;
        boolean inColBounds = 0 <= position.col && position.col < this.cols;
        return inRowBounds && inColBounds;
    }

    public void setValue(int row, int col, T value) {
        ArrayList<T> matrixRow = this.matrix.get(row);
        matrixRow.set(col, value);
    }

    public T getValue(int row, int col) {
        return this.matrix.get(row).get(col);
    }

    public int getRows() {
        return this.rows;
    }

    public int getCols() {
        return this.cols;
    }

    public void print() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return this.toString(true);
    }

    public String toString(boolean pretty) {
        int colSize = this.getMaxLengthFromElementsAsStrings() + 1;
        String formatWithColSpacing = "%" + String.valueOf(colSize) + "s";
        String representation = "";

        if (pretty) {
            representation += "+" + "-".repeat(colSize * this.cols + 1) + "+\n";
        }

        for (int row = 0; row < this.rows; row++) {

            if (pretty) {
                representation += "|";
            }

            for (int col = 0; col < this.cols; col++) {
                T value = this.getValue(row, col);
                String valueRepresentation;

                if (value != null) {
                    valueRepresentation = value.toString();
                } else {
                    valueRepresentation = this.nullRepresentation;
                }

                representation += String.format(formatWithColSpacing, valueRepresentation);
            }

            if (pretty) {
                representation += " |";
            }

            representation += "\n";
        }

        if (pretty) {
            representation += "+" + "-".repeat(colSize * this.cols + 1) + "+\n";
        }

        return representation;
    }

    private int getMaxLengthFromElementsAsStrings() {
        int maxLength = 0;
        ArrayList<T> allElements = this.getAllElements();

        for (int index = 0; index < allElements.size(); index++) {
            T value = allElements.get(index);
            int length;

            if (value != null) {
                length = value.toString().length();
            } else {
                length = this.nullRepresentation.length();
            }

            maxLength = Math.max(length, maxLength);
        }

        return maxLength;
    }

    private ArrayList<T> getAllElements() {
        ArrayList<T> allElements = new ArrayList<T>(this.rows * this.cols);

        for (int row = 0; row < this.rows; row++) {
            allElements.addAll(this.matrix.get(row));
        }

        return allElements;
    }

    // Static Methods
    public static Matrix.Direction pickRandomDirection() {
        Random picker = new Random();
        Matrix.Direction[] options = Matrix.Direction.values();
        return options[picker.nextInt(options.length)];
    }
}
