package com.unicamp.mc322.lab07;

abstract class Obstacle extends InteractiveItem {

    protected Position[] positions;

    // Constructor
    Obstacle(String symbol) {
        super(symbol);
    }

    // Methods
    public Position[] getPositions() {
        return this.positions;
    }

    protected void checkPositionsForNegativeCoordinates() {
        for (Position position: this.positions) {
            if (position.hasNegativeCoordinates()) {
                throw new IllegalArgumentException("Obstacle with negative coordinate: " + position);
            }
        }
    }

    @Override
    public boolean doesGameEnd() {
        return true;
    }
}
