package com.unicamp.mc322.lab07;

class PoisonousFrog extends Frog {

    // Constructor
    PoisonousFrog(String symbol, String name, int row, int col) {
        super(symbol, name, row, col);
    }

    // Methods
    @Override
    public Position whereToMove(Matrix.Direction direction) {
        int[] possibleRanges = new int[]{1, 2, 3, 4};
        int range = Frog.pickRandomRange(possibleRanges);
        Matrix.Direction newDirection = Matrix.pickRandomDirection();
        Position moveVariation = newDirection.variation.scaledBy(range);
        return this.currentPosition.add(moveVariation);
    }

    @Override
    protected double calculatePoints(double rawPoints) {
        return 1.20 * rawPoints;
    }
}
