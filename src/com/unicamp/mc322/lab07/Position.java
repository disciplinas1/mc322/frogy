package com.unicamp.mc322.lab07;

class Position {

    // Properties
    int row;
    int col;

    // Constructor
    Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    // Methods
    public int manhattanDistance(Position other) {
        Position diff = this.subtract(other);
        return diff.manhattanNorm();
    }

    public int manhattanNorm() {
        return Math.abs(this.row) + Math.abs(this.col);
    }

    public boolean isEightConnectedWith(Position other) {
        Position diff = this.subtract(other);
        int maxDiff = Math.max(Math.abs(diff.row), Math.abs(diff.col));
        return maxDiff == 1;
    }

    public Position scaledBy(int value) {
        return new Position(value * this.row, value * this.col);
    }

    public Position add(Position other) {
        return new Position(this.row + other.row, this.col + other.col);
    }

    public Position subtract(Position other) {
        return new Position(this.row - other.row, this.col - other.col);
    }

    public boolean hasNegativeCoordinates() {
        return row < 0 || col < 0;
    }

    @Override
    public String toString() {
        return "(" + this.row + ", " + this.col + ")";
    }
}
