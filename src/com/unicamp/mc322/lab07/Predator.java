package com.unicamp.mc322.lab07;

class Predator extends Obstacle {

    // Static Properties
    private static String symbol = "$$";

    // Constructors
    Predator(int row, int col) {
        super(Predator.symbol);
        this.positions = new Position[1];
        this.positions[0] = new Position(row, col);
        this.checkPositionsForNegativeCoordinates();
    }

    Predator(int row1, int col1, int row2, int col2) {
        super(Predator.symbol);
        this.positions = new Position[2];
        this.positions[0] = new Position(row1, col1);
        this.positions[1] = new Position(row2, col2);
        this.checkIfPositionsAreEightConnected();
        this.checkPositionsForNegativeCoordinates();
    }

    // Methods
    private void checkIfPositionsAreEightConnected() {
        if (this.positions.length == 2) {
            if (!this.positions[0].isEightConnectedWith(this.positions[1])) {
                throw new IllegalArgumentException("Invalid positions for Predator: " + this.positions[0] + " and " + this.positions[1] + " are not 8-connected");
            }
        }
    }

    @Override
    public void interactWith(Frog frog) {
        System.out.println("Eek! The predator got you");
    }
}
