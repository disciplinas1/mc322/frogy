package com.unicamp.mc322.lab07;

class Rock extends Obstacle {

    // Static Properties
    private static String symbol = "<>";

    // Constructor
    Rock(int row, int col) {
        super(Rock.symbol);
        this.positions = new Position[1];
        this.positions[0] = new Position(row, col);
        this.checkPositionsForNegativeCoordinates();
    }

    @Override
    public void interactWith(Frog frog) {
        System.out.println("Ouch! You striked a hard rock");
    }
}
