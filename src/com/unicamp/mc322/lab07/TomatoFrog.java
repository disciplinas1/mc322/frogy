package com.unicamp.mc322.lab07;

class TomatoFrog extends Frog {

    // Constructor
    TomatoFrog(String symbol, String name, int row, int col) {
        super(symbol, name, row, col);
    }

    // Methods
    @Override
    public Position whereToMove(Matrix.Direction direction) {
        int[] possibleRanges;

        switch (direction) {
            case UP:
                possibleRanges = new int[]{2, 3};
                break;

            case DOWN:
                possibleRanges = new int[]{1, 2, 4};
                break;

            case LEFT:
                possibleRanges = new int[]{1, 2, 3};
                break;

            case RIGHT:
                possibleRanges = new int[]{1, 2, 3};
                break;

            default:
                possibleRanges = new int[]{1};
                break;
        }

        int range = Frog.pickRandomRange(possibleRanges);
        Position moveVariation = direction.variation.scaledBy(range);
        return this.currentPosition.add(moveVariation);
    }

    @Override
    protected double calculatePoints(double rawPoints) {
        return 1.10 * rawPoints;
    }
}
