package com.unicamp.mc322.lab07;

class Trap extends Obstacle {

    // Static Properties
    private static String symbol = "{}";

    // Constructors
    Trap(int row, int col) {
        super(Trap.symbol);
        this.positions = new Position[1];
        this.positions[0] = new Position(row, col);
        this.checkPositionsForNegativeCoordinates();
    }

    Trap(int row1, int col1, int row2, int col2) {
        super(Trap.symbol);
        this.positions = new Position[2];
        this.positions[0] = new Position(row1, col1);
        this.positions[1] = new Position(row2, col2);
        this.checkPositionsForNegativeCoordinates();
        this.checkIfPositionsAreCloseEnough();
    }

    Trap(int row1, int col1, int row2, int col2, int row3, int col3) {
        super(Trap.symbol);
        this.positions = new Position[3];
        this.positions[0] = new Position(row1, col1);
        this.positions[1] = new Position(row2, col2);
        this.positions[2] = new Position(row3, col3);
        this.checkPositionsForNegativeCoordinates();
        this.checkIfPositionsAreCloseEnough();
    }

    // Methods
    private void checkIfPositionsAreCloseEnough() {
        int nPositions = this.positions.length;

        for (int i = 0; i < nPositions - 1; i++) {
            for (int j = i + 1; j < nPositions; j++) {
                int distance = this.positions[i].manhattanDistance(this.positions[j]);
                boolean tooFar = distance > 2;
                boolean samePosition = distance == 0;
                if (tooFar || samePosition) {
                    throw new IllegalArgumentException("Invalid positions for Trap: " + this.positions[i] + " and " + this.positions[j]);
                }
            }
        }
    }

    @Override
    public void interactWith(Frog frog) {
        System.out.println("Damn! Didn't see this trap coming?");
    }
}
